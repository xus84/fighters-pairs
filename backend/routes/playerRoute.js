const express = require("express")
const router = express.Router()
const Player = require("../models/playerModel")

router.route("/create").post((req, res) => {
        const content = req.body.content
        const newPlayer = new Player({
                content
        })

        newPlayer.save()
})

module.exports = router;