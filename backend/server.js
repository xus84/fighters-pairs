const express = require("express")
const app = express()
const cors = require("cors")
const mongoose = require("mongoose")

app.use(cors())
app.use(express.json())

mongoose.connect('mongodb://localhost:27017/playersDB')

app.use("/", require("./routes/playerRoute"))

app.listen(4000, ()=> {
        console.log("Server is running on port 4000")
})