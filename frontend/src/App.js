import { useState, useEffect } from "react";
import "./App.css";
import Form from "./Form";

function App() {
  const [nameItems, setNameItems] = useState([]);


  function createPlayer(playerName){
        setNameItems([...nameItems, {name: playerName}])
  }

  useEffect(()=>{
      let data=   localStorage.getItem('players') 
        if(data) {
                setNameItems(JSON.parse(data))
        }
}, [])

  useEffect(()=>{
        localStorage.setItem('players', JSON.stringify(nameItems))
  }, [nameItems])

  return (
    <div className="App">
      <Form createPlayer={createPlayer} />

      <table>
        <thead>
          <tr>
            <th>PLAYERS</th>
          </tr>
        </thead>
        <tbody>
          {nameItems.map((nameItem) => (
            <tr key={nameItem.name}>
              <td>{nameItem.name}<button className="deleteBtn"><span>X</span></button></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default App;
