import { useState} from "react";
 import axios from "axios"
import "./Form.css";

export default function Form({createPlayer}) {
  const [player, setPlayer] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    createPlayer(player)
    const newPlayer = {
        title: player,
        content: player,
}
axios.post('http://localhost:4000/create', newPlayer)
    localStorage.setItem("player", player);
    setPlayer("");
  };

  return (
    <div className="App">
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Enter a Player"
          value={player}
          onChange={(e) => setPlayer(e.target.value)}
        />

        <button>Save Player</button>
      </form>
    </div>
  );
}
